import numpy as np


def is_zeros(array):
    for elem in array:
        if elem != 0 or elem != 0.:
            return False
    return True


def get_dividers(array, divider):
    res = []
    if divider < 0:
        for i in range(len(array)):
            if array[i] < 0.:
                res.append(abs(array[i] // abs(divider)))
            else:
                res.append(-1*(-array[i] // divider))
        return res
    else:
        return array // divider


def get_divider(number, divider):
    if divider < 0:
        if number < 0.:
            return abs(number // abs(divider))
        else:
            return -1*(-number // divider)
    else:
        return number // divider


def tril(matrix):
    for i in range(np.shape(matrix)[0]):
        while not is_zeros(matrix[i][i+1:]):
            matrix[i:, i:] = sort(matrix[i:, i:])
            dividers = get_dividers(matrix[i][i+1:], matrix[i][i])
            for j, div in enumerate(dividers):
                matrix[:, (i+1) + j] -= matrix[:, i] * div
    return matrix


def sort(matrix):
    k = np.shape(matrix)[1] - 1
    while k > 0:
        ind = 0
        for j in range(1, k + 1):
            if abs(matrix[0][j]) > abs(matrix[0][ind]) > 0:  # ищем столбец с макс значением
                ind = j
        N = np.shape(matrix)[0]
        for i in range(N):  # перемещаем максимальный столбец в конец матрицы
            b = matrix[i][ind]
            matrix[i][ind] = matrix[i][k]
            matrix[i][k] = b
        k -= 1
    nulls = 0
    N = np.shape(matrix)[1]
    for i in range(N):
        if not matrix[0][i] == 0:
            break
        nulls += 1

    if nulls > 0:
        for i in range(np.shape(matrix)[0]):  # перемещаем максимальный столбец в конец матрицы
            b = matrix[i][nulls]
            matrix[i][nulls] = matrix[i][0]
            matrix[i][0] = b

    return matrix
