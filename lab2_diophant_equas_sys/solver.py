import numpy as np
import numerpy


def can_resolve(a_values, m):
    is_could_resolve = 1
    for j in range(m):
        is_could_resolve &= (a_values[j][0] == 0)
    return is_could_resolve


def get_rang(matrix):
    r = 0
    end = np.shape(matrix)[0] - np.shape(matrix)[1]
    for i in range(end):
        if matrix[i][i] > 0:
            r = i
        else:
            break
    return r + 1


def resolve(a_matrix, b_vec):
    a_matrix = np.array(a_matrix)
    b_vec = np.array(b_vec)

    n = len(a_matrix[0])  
    m = len(a_matrix)  

    not_b_vec = [[(-1) * x[0]] for x in b_vec]  
    zero_vec = [[0] for i in range(n)] 
    e_matrix = np.identity(n, dtype=int)

    a_e_matrixes = np.concatenate((a_matrix, e_matrix), axis=0)
    c_k_matrixes = numerpy.tril(a_e_matrixes)

    r = get_rang(c_k_matrixes)
    a_values = np.concatenate([not_b_vec, zero_vec], axis=0)

    k = 0
    while k < r and c_k_matrixes[k][k] != 0:
        k_i = a_values[k][0] // c_k_matrixes[k][k]
        a_values[:, 0] = a_values[:, 0] - k_i * c_k_matrixes[:, k]
        k += 1

    if can_resolve(a_values, m):
        s = n - r
        x_0 = a_values[m:, :]
        k_vectors = c_k_matrixes[m:, r - 1:]
        k_vectors_sum_str = ""
        for vec in ["t_" + str(i + 1) + "*" + str(np.reshape(k_vectors[:, i + 1], (-1, 1)).T) for i in range(s)]:
            k_vectors_sum_str += vec
            k_vectors_sum_str += " + "
        # print("Particular solution is ", x_0.T, " + ", k_vectors_sum_str[:-3])
        return True, x_0
    else:
        # print("System cannot be resolved in whole numbers!")
        return False, a_values[m:, :]


def main():
    resolve(
        # [[27, 19, -35, -50, -6, -8, -25, -16],
        # [44, -28, -42, -12, 38, -50, -42, 13],
        #  [-6, -11, 25, -11, 12, 32, -6, -29],
        #  [-45, -42, 29, 10, -30, -10, -38, 32]],
        # [[-4],
        # [-48],
        #  [-29],
        # [-8]]
        # [[2, -11, 13, 5, 5],
        # [62, 22, -73, 7, 7],
        # [2, 5, -1, 8, 9]],
        # [[1],
        # [-31],
        # [-1]]

    )


if __name__ == '__main__':
    main()

