import unittest
import numpy as np
import solver


def generate_example(m, n):
    matrix = np.random.randint(low=-50, high=50, size=(m, n))
    values = np.random.randint(low=-50, high=50, size=(m, 1))
    return matrix, values


def generate_dims():    # returns m and n : m < n
    m = np.random.randint(3, 10)
    n = np.random.randint(3, 10)
    if m < n:
        return m, n
    else:
        return n, m


def substitute(matrix, values):
    res = []
    for i in range(np.shape(matrix)[0]):
        res.append(np.dot(values[:, 0], matrix[i]))
    return res


class TestDiofantSolver(unittest.TestCase):
    def test_solver(self):
        passed = 0
        not_resolved = 0
        for i in range(100):
            m, n = generate_dims()
            matrix, values = generate_example(m, n)
            is_resolved, part_solution = solver.resolve(matrix, values)
            if is_resolved:
                values_expected = substitute(matrix, part_solution)
                if (values[:, 0] == values_expected).all():
                    print(i, "- Passed!")
                    passed += 1
                else:
                    print(i, "- Failed...")
            else:
                not_resolved += 1
                print(i, "- Cannot be resolved...")
        print("Passed : ", passed, " / ", 100, ".")
        print("Cannot be resolved : ", not_resolved, " / ", 100, ".")
        print("Failed : ", 100 - (passed + not_resolved), " / ", 100, ".")


if __name__ == '__main__':
    unittest.main()
