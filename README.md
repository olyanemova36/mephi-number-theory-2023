# mephi-number-theory-2023


## Описание
Репозиторий для курса "Основы теории чисел".

| Название лаборатной              | Язык        | Статус        | 
| -------------------              | ------------- | ------------- | 
| lab1_euclidean_algo              | Rust  1.67.1     |       ✅      | 
| lab2_diophant_equas_sys          | Python 3.7       |       ✅      | 
| lab3_easy_diff_cryptoanalysis    | C# 10        |         ?     | 
