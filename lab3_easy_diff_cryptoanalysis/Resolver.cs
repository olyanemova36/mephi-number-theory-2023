using System.Collections;
using System.Text;

namespace Dotnet.Olympic.CryptoFox;

public class Resolver
{
    static Resolver()
    {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
    }

    private static IEnumerable<byte[]> PossibleFirstPlainTexts
    {
        get
        {
            var alphabet = Encoding.GetEncoding(1251).GetBytes("абвгдежзийклмнопрстуфхцчшщъыьэюя;.,");
            return
                from b1 in alphabet
                from b2 in alphabet
                select new[] {b1, b2};
        }
    }

    public static void FindPlainTexts(List<byte[]> cipherText)
    {
        var gammasXorCiphers = GetGammasXorCiphers(cipherText).ToArray();
        var plains = PossibleFirstPlainTexts.Select(p1 =>
        {
            var plains = gammasXorCiphers // got {p2, p3, p4, ..., p12} and p1 - enumerated
                .Select(gammaXorCipher => gammaXorCipher.Xor(p1))
                .ToList();
            plains.Insert(0, p1); // got {p1, p2, p3, p4, ..., p12}
            return plains;
        });

        using var writer = new StreamWriter(@"plainTexts.txt");
        foreach (var plain in plains)
        {
            writer.WriteLine(
                $"Possible plain text: {Encoding.GetEncoding(1251).GetString(plain.SelectMany(pt => pt).ToArray())}");
        }
    }

    private static IEnumerable<byte[]> GetGammasXorCiphers(List<byte[]> ciphertext) // d1-2, d1-3, d1-4, d1-5, d1-6, d1-7, ... , d10-11, d10-12, d11-12
    {
        for (byte i = 0x01; i <= 0x0b; i++) // 0b = 11
        {
            var inputGammaDiff = new byte[] {0x00, i}.Xor(new byte[] {0x00, (byte) (i + 1)});
            var cipherDiff = ciphertext[i - 1].Xor(ciphertext[i]); // delCi_k 
            for (var j = 0; j < 8; j++) // calculate delGi-k
            {
                inputGammaDiff = PerNibbles(SubNibbles(inputGammaDiff));
            }

            yield return inputGammaDiff.Xor(cipherDiff); // add delCi_k xor delGi_k
        }
    }

    private static byte[] PerNibbles(byte[] block)
    {
        var bits = new BitArray(block);
        var byte1 = new BitArray(8);
        var byte2 = new BitArray(8);

        byte1[0] = bits[12];
        byte1[1] = bits[0];
        byte1[2] = bits[13];
        byte1[3] = bits[5];
        byte1[4] = bits[6];
        byte1[5] = bits[9];
        byte1[6] = bits[11];
        byte1[7] = bits[10];
        byte2[0] = bits[4];
        byte2[1] = bits[14];
        byte2[2] = bits[8];
        byte2[3] = bits[15];
        byte2[4] = bits[7];
        byte2[5] = bits[1];
        byte2[6] = bits[3];
        byte2[7] = bits[2];

        return new[] {GetByte(byte1), GetByte(byte2)};
    }

    private static byte GetByte(BitArray input)
    {
        int len = input.Length;
        if (len > 8)
            len = 8;
        int output = 0;
        for (int i = 0; i < len; i++)
            if (input.Get(i))
                output += (1 << (len - 1 - i)); //this part depends on your system (Big/Little)
        //output += (1 << i); //depends on system
        return (byte) output;
    }

    private static byte[] SubNibbles(byte[] block) => block.Select(SBox).ToArray();

    private static byte SBox(byte @byte) => (byte) (SubNibblesHigh((byte) (@byte / 16 * 16)) +
                                                    SubNibblesLower((byte) (@byte % 16)));

    private static byte SubNibblesHigh(byte @byte) => @byte switch
    {
        0x00 => 0x00,
        0x10 => 0x80,
        0x20 => 0x40,
        0x30 => 0xc0,
        0x40 => 0x20,
        0x50 => 0xa0,
        0x60 => 0x60,
        0x70 => 0xe0,
        0x80 => 0x10,
        0x90 => 0x90,
        0xa0 => 0x50,
        0xb0 => 0xd0,
        0xc0 => 0x30,
        0xd0 => 0xb0,
        0xe0 => 0x70,
        0xf0 => 0xf0,
        _ => throw new ArgumentOutOfRangeException(nameof(@byte), @byte, null)
    };

    private static byte SubNibblesLower(byte @byte) => @byte switch
    {
        0x00 => 0x00,
        0x01 => 0x08,
        0x02 => 0x04,
        0x03 => 0x0c,
        0x04 => 0x02,
        0x05 => 0x0a,
        0x06 => 0x06,
        0x07 => 0x0e,
        0x08 => 0x01,
        0x09 => 0x09,
        0x0a => 0x05,
        0x0b => 0x0d,
        0x0c => 0x03,
        0x0d => 0x0b,
        0x0e => 0x07,
        0x0f => 0x0f,
        _ => throw new ArgumentOutOfRangeException(nameof(@byte), @byte, null)
    };
}
