namespace Dotnet.Olympic.CryptoFox;

public static class ByteExtensions
{
    public static byte[] Xor(this byte[] first, byte[] second)
    {
        if (IsTermsValid(first, second))
        {
            var lenght = first.Length;
      
            for (var i = 0; i < lenght; ++i)
            {
                first[i] = (byte) (first[i] ^ second[i]);
            }

            return first;
        }

        throw new NotSupportedException("The method can't support \"XOR\" with not valid bytes");
    }

    private static bool IsTermsValid(IReadOnlyCollection<byte> first, IReadOnlyCollection<byte> second) => first.Count == second.Count;
}
