using Dotnet.Olympic.CryptoFox;

var cipherTexts = new List<List<byte[]>>
{
    new List<byte[]>{
        new byte[]{ 0xf9,0x92 },
        new byte[]{ 0x88, 0xfd },
        new byte[]{ 0x10, 0x8f },
        new byte[]{ 0xf3, 0xcf },
        new byte[]{ 0xe6, 0xdb },
        new byte[]{ 0x00, 0xd3 },
        new byte[]{ 0xbd, 0xf6 },
        new byte[]{ 0xce, 0x38 },
        new byte[]{ 0xba, 0xe1 },
        new byte[]{ 0xd6, 0xaf },
        new byte[]{ 0xc2, 0x27 },
        new byte[]{ 0x20, 0x54 }
    },
    new List<byte[]>
    {
        new byte[]{ 0xfa, 0x89 },
        new byte[]{ 0x8d, 0xf2 },
        new byte[]{ 0xd8, 0x43 },
        new byte[]{ 0xe1, 0xc9 },
        new byte[]{ 0xed, 0xc3 },
        new byte[]{ 0xcd, 0xc5 },
        new byte[]{ 0xbd, 0x37 },
        new byte[]{ 0xca, 0xe8 },
        new byte[]{ 0xa3, 0xe3 },
        new byte[]{ 0xc7, 0x71 },
        new byte[]{ 0x10, 0x2b },
        new byte[]{ 0x20, 0x54 }
    },
    new List<byte[]>
    {
        new byte[]{ 0xea, 0x8f },
        new byte[]{ 0x54, 0xe8 },
        new byte[]{ 0xd0, 0x89 },
        new byte[]{ 0x30, 0xc3 },
        new byte[]{ 0xea, 0xc0 },
        new byte[]{ 0xd0, 0xc2 },
        new byte[]{ 0x74, 0xf9 },
        new byte[]{ 0xcd, 0xf8 },
        new byte[]{ 0x70, 0xf6 },
        new byte[]{ 0xd8, 0xa8 },
        new byte[]{ 0xde, 0xee },
        new byte[]{ 0xf2, 0x58 }
    },
    new List<byte[]>
    {
        new byte[]{ 0xea, 0x8f },
        new byte[]{ 0x58, 0xe5 },
        new byte[]{ 0xd0, 0x8c },
        new byte[]{ 0xfb, 0xc4 },
        new byte[]{ 0xff, 0xc0 },
        new byte[]{ 0xd2, 0x12 },
        new byte[]{ 0x78, 0xfd },
        new byte[]{ 0xc0, 0xf2 },
        new byte[]{ 0x70, 0xf5 },
        new byte[]{ 0xd0, 0xb8 },
        new byte[]{ 0xcf, 0x25 },
        new byte[]{ 0x20, 0x54 }
    }
};

foreach (var cipherText in cipherTexts)
{
    Resolver.FindPlainTexts(cipherText);
}







