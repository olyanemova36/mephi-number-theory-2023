# Euclidean Algorithm

Is a Rust CLI application for Least Common Multiple and Greatest Common Divisor finding.

## Usage

Use the cargo compiler in source directory [cargo](https://doc.rust-lang.org/cargo/) to run the progect.

```bash
cargo run

Input "a":
 
# User's input from console
2
your integer input: 2

Input "b":
# User's input from console
18
your integer input: 18

Least Common Multiple: 18
Greatest Common Divisor: 2

```
