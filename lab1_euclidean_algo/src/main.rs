use std::io;

fn main(){

    let mut _a: i64 = read_i64("a").abs();
    let mut _b: i64 = read_i64("b").abs();

    println!("Least Common Multiple: {}", lcm(_a, _b));
    println!("Greatest Common Divisor: {}", gcd(_a, _b));
}

fn read_i64( var: &str) -> i64 {
    println!("Input \"{var}\": ");

    let mut _input = String::new();
    io::stdin()
        .read_line(&mut _input)
        .expect("failed to read from stdin");

    let _trimmed = _input.trim();
    let mut _result : i64 = 0;

    match _trimmed.parse::<i64>() {
        Ok(i) => {
            println!("your integer input: {i}");
            _result = i;
        },
        Err(..) => println!("this was not an integer: {_trimmed}"),
    };
    _result
}

fn lcm( a: i64, b: i64) -> i64 {
    (a*b) / gcd(a,b)
}

fn gcd( a: i64, b: i64) -> i64 {
    if a == 0 {
        b
    } else if b == 0 {
        a
    } else if a > b {
        gcd(a % b, b)
    } else {
        gcd(b % a, a)
    }
}

#[cfg(test)]
mod tests {
    use super::{lcm,gcd};

    #[test]
    fn test_lcm() {
        assert_eq!(lcm(2, 1024), 1024);
        assert_eq!(lcm(13, 21), 273);
    }

    #[test]
    fn test_gcd() {
        assert_eq!(gcd(-30, 180), -30);
        assert_eq!(gcd(13, 21), 1);
    }

    #[test]
    #[ignore]
    fn ignored_test() {
        assert_eq!(gcd(0, 0), 0);
        assert_eq!(lcm(0, 0), 0);
    }
}
